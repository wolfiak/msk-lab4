package com.example.pacio.msk_lab4;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toast t=new Toast(getApplicationContext());
        t.setDuration(Toast.LENGTH_LONG);
        t.setGravity(Gravity.AXIS_PULL_BEFORE,10,-10);

      //  t.show();
        View tost=getLayoutInflater().inflate(R.layout.tost,null);
        TextView tv=tost.findViewById(R.id.textView);
        tv.setText("Dupa kamieni kupa");
        t.setView(tost);
        t.show();

        Dialogo d=new Dialogo();
        d.show(getFragmentManager(),"dialog");
        Intent i=new Intent(this,MainActivity.class);
        PendingIntent pi=PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder b= new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Elo ")
                .setContentText("gitara siema")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pi);

        NotificationManager nm= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify("not",1,b.build());

    }
}
